// Find the sum of all salaries.

const convertToNumbers = require("./problem2.js");

function sumOfAllSalaries(Person_Database) {
    if (Array.isArray(Person_Database)) {

        let updated_Database = convertToNumbers(Person_Database);
        let sumOfSalaries = 0;
        for (const person of updated_Database) {
            sumOfSalaries += person.salary;

        }
        return sumOfSalaries;

    }
    else {
        return null;
    }

}
module.exports = sumOfAllSalaries;