// Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

const convertToNumbers = require("./problem2.js")

function addNewKey(Person_Database) {
    if (Array.isArray(Person_Database)) {

        for (const person of convertToNumbers(Person_Database)) {
            person.corrected_salary = person.salary * 10000;
        }
        return Person_Database;

    }
    else {
        return null;
    }


}
module.exports = addNewKey;