// Convert all the salary values into proper numbers instead of strings.

function convertToNumbers(Person_Database) {
    if (Array.isArray(Person_Database)) {
        for (const person of Person_Database) {
            let salary = person.salary;
            let numericalValue = Number(subString(salary,1,salary.length));
            person.salary = numericalValue
        }
        return Person_Database;
    }
    else {
        return null;
    }
}
function subString(salary,start,end)
{
    let requiredString = "";
    for(let index = start;index<end;index++)
    {
        requiredString += salary.charAt(index);

    }
    return requiredString;
}
module.exports = convertToNumbers