// Find the average salary of based on country.

const sumOfSalariesByCountry = require("./problem5.js");

function averageSalaryByCountry(Person_Database) {
    if (Array.isArray(Person_Database)) {
        let personCountPerCountry = {}; // Object to count person per country;


        for (const person of Person_Database) {

            if (personCountPerCountry[person.location]) {
                personCountPerCountry[person.location] = personCountPerCountry[person.location] + 1;
            }
            else {
                personCountPerCountry[person.location] = 1;
            }
        }

        let salaryByCountry = sumOfSalariesByCountry(Person_Database);
        let averageSalaries = {};
        for (const country in salaryByCountry) {
            averageSalaries[country] = (salaryByCountry[country] / personCountPerCountry[country]);
        }
        return averageSalaries;
    }
    else {
        return null;
    }
}
module.exports = averageSalaryByCountry;


