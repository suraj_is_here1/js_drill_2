// Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
function findwebDevelopers(Person_Database) {
    if (Array.isArray(Person_Database)) {
        let webDevelopers = [];
        for (const person of Person_Database) {
            if (person.job.startsWith("Web Developer")) {
                webDevelopers.push(person);
            }

        }
        return webDevelopers;
    }
    else {
        return null;
    }


}
module.exports = findwebDevelopers;