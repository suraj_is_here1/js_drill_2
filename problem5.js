// Find the sum of all salaries based on country.
const convertToNumbers = require("./problem2.js");

function sumOfSalariesByCountry(Person_Database) {
    if (Array.isArray(Person_Database)) {
        let updated_Database = convertToNumbers(Person_Database);
        let salaryByCountry = {}; // Object to contain country with it's respective salary.

        for (const person of updated_Database) {

            if (salaryByCountry[person.location]) {
                salaryByCountry[person.location] = salaryByCountry[person.location] + person.salary
            }
            else {
                salaryByCountry[person.location] = person.salary;
            }
        }
        return salaryByCountry;
    }
    else {
        return null

    }
}
module.exports = sumOfSalariesByCountry;